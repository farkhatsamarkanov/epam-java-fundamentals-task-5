package com.epam.javafundamentals.model;

import java.util.ArrayList;

public class Pizza {
    private final static int MAXIMUM_NUMBER_OF_INGREDIENTS = 7;            //constants
    private String pizzaName;                                               //pizza properties
    private int pizzaCount;
    private ArrayList<Ingredient> pizzaIngredients;

    public Pizza (String pizzaName, boolean isCalzone, int pizzaCount){    //pizza constructor
        pizzaIngredients = new ArrayList<>();
        this.pizzaName = pizzaName;
        this.pizzaCount = pizzaCount;
        if (isCalzone) {
            pizzaIngredients.add(Ingredient.PIZZA_TYPE_CALSONE);
        }
        else {
            pizzaIngredients.add(Ingredient.PIZZA_TYPE_DEFAULT);
        }
    }

    public String getPizzaName() {                                          //getters
        return pizzaName;
    }

    public int getPizzaCount() {
        return pizzaCount;
    }

    public ArrayList<Ingredient> getPizzaIngredients() {
        return pizzaIngredients;
    }

    protected void setPizzaName(String pizzaName) {                        //setters
        this.pizzaName = pizzaName;
    }

    protected void setPizzaCount(int pizzaCount) {
        this.pizzaCount = pizzaCount;
    }

    public double calculatePizzaCost(){                                   //method to calculate a cost of one pizza
        if (pizzaIngredients == null) {
            return 0;
        } else {
            double cost = 0;
            for (Ingredient ingredient: pizzaIngredients){
                cost += ingredient.getPrice();
            }
            return cost;
        }
    }

    public void addIngredient(Ingredient newIngredient){                  //method to add ingredient to pizza
        if (pizzaIngredients.size() > MAXIMUM_NUMBER_OF_INGREDIENTS){      //checking if pizza is "full"
            System.out.println("You can't add more than "+ MAXIMUM_NUMBER_OF_INGREDIENTS + " ingredients!");
        } else {
            for (Ingredient ingredient : pizzaIngredients){                //looping through pizza's ingredients
                if (ingredient.getName().equals(newIngredient.getName())){ //checking if ingredient is already in a pizza
                    System.out.println(newIngredient.getName() + " is already in the pizza!");
                    return;
                }
            }
            pizzaIngredients.add(newIngredient);
        }
    }

    public String toString(){
        String borderline = String.format("%32s", " ").replace(' ', '-') + "\n";
        StringBuilder stringToReturn = new StringBuilder("Name: ");
        stringToReturn.append(pizzaName)
                      .append("\n")
                      .append(borderline);
        for (Ingredient ingredient: pizzaIngredients){
            stringToReturn.append(String.format("%-27s",ingredient.getName()))
                    .append(String.format("%.2f",ingredient.getPrice()))
                    .append(" €\n");
        }
        return stringToReturn.append(borderline)
                .append("Total:")
                .append(String.format("%25.2f", calculatePizzaCost()))
                .append(" €\n")
                .append("Count:")
                .append(String.format("%27d", pizzaCount))
                .append("\n")
                .append(borderline)
                .toString();
    }
}
