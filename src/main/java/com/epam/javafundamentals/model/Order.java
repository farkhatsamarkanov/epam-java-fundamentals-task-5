package com.epam.javafundamentals.model;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import com.epam.javafundamentals.validation.PizzaNameValidator;

public class Order {
    public static final int MAXIMUM_NUMBER_OF_PIZZAS = 10;                            //constants
    private static int orderCount = 10000;                                             //order properties
    private final int ORDER_ID;
    private final int CLIENT_ID;
    private ArrayList<Pizza> pizzaArray;
    private LocalTime localTime;
    private DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
    private PizzaNameValidator pizzaNameValidator = new PizzaNameValidator();

    public Order(int clientId) {                                                        //constructor
        pizzaArray = new ArrayList<>();
        ORDER_ID = ++orderCount;
        this.CLIENT_ID = clientId;
        localTime = LocalTime.now();
    }

    public int getOrderId() {
        return ORDER_ID;
    }                                       //getters

    public int getClientId() {
        return CLIENT_ID;
    }

    public int countPizzasInOrder() {                                                    //method to count pizzas added to order
        if (pizzaArray == null) {
            return 0;
        } else {
            int pizzaCount = 0;
            for (Pizza pizza : pizzaArray) {
                pizzaCount += pizza.getPizzaCount();
            }
            return pizzaCount;
        }
    }

    public double calculateTotalCost() {                                                 //method to calculate total cost of pizzas
        if (pizzaArray == null) {
            return 0;
        } else {
            double totalCost = 0;
            for (Pizza pizza : pizzaArray) {
                totalCost += pizza.calculatePizzaCost() * pizza.getPizzaCount();
            }
            return totalCost;
        }
    }


    public boolean addPizza(Pizza pizza) {                                               //method to add pizza in an order
        if (countPizzasInOrder() + pizza.getPizzaCount() > MAXIMUM_NUMBER_OF_PIZZAS) {
            System.out.println("You can't order more than " + MAXIMUM_NUMBER_OF_PIZZAS + " pizzas!");
            return false;
        } else {
            if (!pizzaNameValidator.isValidPizzaName(pizza.getPizzaName())) {
                pizza.setPizzaName(CLIENT_ID + "_" + (pizzaArray.size() + 1));
            }
            pizzaArray.add(pizza);
            System.out.println(pizza.getPizzaCount() + " " + "\"" + pizza.getPizzaName() + "\" pizza(s) has(ve) been added to the order!");
            printPizzaAttributes(pizza);
            return true;
        }
    }

    public boolean deletePizza(String pizzaName) {                                       //method to delete pizza from an order
        for (Pizza pizza : pizzaArray) {
            if (pizza.getPizzaName().equals(pizzaName)) {
                System.out.println("Pizza " + "\"" + pizzaName + "\"" + " has been deleted from the order!");
                return (pizzaArray.remove(pizza));
            }
        }
        System.out.println("No pizza named " + "\"" + pizzaName + "\"" + " in the order! Please try again!");
        return false;
    }

    public boolean editPizzaCount(String pizzaName, int newCount) {                                                    //method to edit count of existing pizza
        for (Pizza pizza : pizzaArray) {                                                                                //looping through array of pizzas
            if (pizza.getPizzaName().equals(pizzaName)) {                                                               //check if pizza exist in an array
                if (countPizzasInOrder() - pizza.getPizzaCount() + newCount > MAXIMUM_NUMBER_OF_PIZZAS) {               //checking if new count is valid
                    System.out.println("You can't order more than " + MAXIMUM_NUMBER_OF_PIZZAS + " pizzas!");
                    return false;
                } else {
                    pizza.setPizzaCount(newCount);
                    printPizzaAttributes(pizza);
                    return true;
                }
            }
        }
        System.out.println("No pizza named " + "\"" + pizzaName + "\"" + " in the order! Please try again!");
        return false;
    }

    public boolean editPizzaName(String pizzaName, String newPizzaName) {                                             //method to edit name of an existing pizza
        for (Pizza pizza : pizzaArray) {                                                                                //looping through array of pizzas and checking if pizza exists
            if (pizza.getPizzaName().equals(pizzaName)) {
                pizza.setPizzaName(newPizzaName);
                printPizzaAttributes(pizza);
                return true;
            }
        }
        System.out.println("No pizza named " + "\"" + pizzaName + "\"" + " in the order! Please try again!");
        return false;
    }

    private void printPizzaAttributes(Pizza pizza) {                                                                  //method to print pizza's attributes
        String stringToPrint = "[" + ORDER_ID + " : " + CLIENT_ID + " : " + pizza.getPizzaName() + " : " + pizza.getPizzaCount() + "]";
        System.out.println(stringToPrint);
    }

    public String toString() {                                                                                          //method to print a check
        String borderline = String.format("%32s", " ").replace(' ', '*') + "\n";
        StringBuilder stringToReturn = new StringBuilder("\n" + borderline);
        stringToReturn.append("Time: ")
                .append(localTime.format(timeFormatter))
                .append("\n")
                .append("Order: ")
                .append(ORDER_ID)
                .append("\n")
                .append("Client: ")
                .append(CLIENT_ID)
                .append("\n");
        if (pizzaArray == null) {
            return stringToReturn.append("Empty order")
                    .append("\n")
                    .append(borderline)
                    .toString();
        } else {
            for (Pizza pizza : pizzaArray) {
                stringToReturn.append(pizza);
            }
            return stringToReturn.append("Total sum:")
                    .append(String.format("%16.2f", calculateTotalCost()))
                    .append(" €")
                    .append("\n")
                    .append(borderline)
                    .toString();
        }
    }
}
