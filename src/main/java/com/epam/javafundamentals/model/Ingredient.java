package com.epam.javafundamentals.model;

public enum Ingredient {                                    //enum containing pizza ingredients
    BACON("Bacon", 1.2),
    CHEESE("Cheese", 1.0),
    CORN("Corn", 0.7),
    GARLIC("Garlic", 0.3),
    OLIVES("Olives", 0.5),
    PEPPERONI("Pepperoni", 0.6),
    PIZZA_TYPE_DEFAULT("Usual base", 1.0),
    PIZZA_TYPE_CALSONE("Calsone base", 1.5),
    SALAMI("Salami", 1.5),
    TOMATO_PASTE("Tomato Paste", 1.0);

    private double price;                                   //ingredient properties
    private String name;

    Ingredient(String name, double price) {                  //constructor
        this.name = name;
        this.price = price;
    }

    public double getPrice() {                               //getters
        return price;
    }

    public String getName() {
        return name;
    }

}
