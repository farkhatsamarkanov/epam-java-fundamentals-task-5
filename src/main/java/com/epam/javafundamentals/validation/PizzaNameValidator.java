package com.epam.javafundamentals.validation;

public class PizzaNameValidator {
    private final static int MAXIMUM_PIZZA_NAME_LENGTH = 20;
    private final static int MINIMUM_PIZZA_NAME_LENGTH = 4;

    public boolean isValidPizzaName(String pizzaName) {        //method to validate pizza's name
        int nameLength = pizzaName.length();
        //check if pizza name has valid length
        return ((nameLength >= MINIMUM_PIZZA_NAME_LENGTH) && (nameLength <= MAXIMUM_PIZZA_NAME_LENGTH));
    }

}
