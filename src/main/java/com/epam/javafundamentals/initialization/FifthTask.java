package com.epam.javafundamentals.initialization;

import com.epam.javafundamentals.model.Ingredient;
import com.epam.javafundamentals.model.Pizza;
import com.epam.javafundamentals.model.Order;

public class FifthTask
{
    public static void main( String[] args )
    {
        Pizza pizzaMargarita = new Pizza("Margarita", true, 2);         //creating pizza object "Margarita"
        pizzaMargarita.addIngredient(Ingredient.GARLIC);                                             //adding ingredients to pizza
        pizzaMargarita.addIngredient(Ingredient.BACON);
        pizzaMargarita.addIngredient(Ingredient.TOMATO_PASTE);
        pizzaMargarita.addIngredient(Ingredient.OLIVES);
        Pizza pizzaPepperoni = new Pizza("Pepperoni", false, 3);        //creating pizza object "Pepperoni"
        pizzaPepperoni.addIngredient(Ingredient.PEPPERONI);                                          //adding ingredients to pizza
        pizzaPepperoni.addIngredient(Ingredient.CHEESE);
        pizzaPepperoni.addIngredient(Ingredient.CORN);
        pizzaPepperoni.addIngredient(Ingredient.SALAMI);
        Order firstOrder = new Order(7717);                                                   //creating order object
        firstOrder.addPizza(pizzaMargarita);                                                         //adding pizzas to firstOrder object
        firstOrder.addPizza(pizzaPepperoni);
        System.out.print(firstOrder + "\n");
        Pizza pizzaZZ = new Pizza("Base ZZ", false, 12);                //creating pizza object "Base ZZ"
        Order secondOrder = new Order(4372);                                                  //creating order object
        secondOrder.addPizza(pizzaZZ);                                                               //adding pizzas to secondOrder object
        System.out.println(secondOrder);
    }
}
